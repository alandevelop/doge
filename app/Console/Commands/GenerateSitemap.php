<?php

namespace App\Console\Commands;

use App\Services\PrismicService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;
use Spatie\Sitemap\Sitemap;

use App\Services\CategoryUrls;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		URL::forceRootUrl('https://doge.ru');
		URL::forceScheme('https');
		$sitemap = Sitemap::create()
			->add('/')
			->add('/poroda')
			->add('/contacts');

    	$prismicService = new PrismicService();

		foreach (CategoryUrls::getUrls() as $category) {
			$slug = $category['slug'];
			$sitemap->add("poroda/$slug");
		}

		foreach ($prismicService->getAllBreeds() as $letter) {
			foreach ($letter as $breed) {
				if(!$breed['is_active']) continue;
				$uid = $breed['uid'];
				$sitemap->add("poroda/$uid");
			}
		}

		foreach ($prismicService->getAllPosts() as $post) {
			$uid = $post->uid;
			$sitemap->add("blog/$uid");
		}

		$sitemap->writeToFile(public_path('sitemap.xml'));
    }
}