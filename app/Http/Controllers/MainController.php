<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Prismic\Api;

use App\Services\PrismicService;

use App\Services\CategoryUrls;

class MainController extends Controller
{
	public $api;

	public function __construct()
	{
		$url = "https://doge.cdn.prismic.io/api/v2";
		$token = config('app.prismic_token');

		$api = Api::get($url, $token);

		$this->api = $api;
	}

	public function index(Request $request)
	{
		$service = new PrismicService();
		$categories = $service->getIndexCategories();
		$popular = $service->getPopularBreeds();
		$posts = $service->getIndexPosts();
		$allBreedsIndex = $service->getAllBreedsIndex();

		return view('pages.index.index', compact('categories', 'popular', 'posts', 'allBreedsIndex'));
	}

	public function breed(Request $request, $uri)
	{
		$service = new PrismicService();

		$data = $service->getBreed($uri);
		$categories = $service->getIndexCategories();
		$popular = $service->getPopularBreeds();

		return view('pages.breed_item.breed_item', compact('data','categories', 'popular'));
	}

	public function breeds(Request $request)
	{
		$service = new PrismicService();

		if(!preg_grep('#.*#', $request->all())) { // if all filters are null
			$breeds = $service->getAllBreeds();
		} else {
			$breeds = $service->getBreedsByFilter($request->all());
		}

		$filters = $service->getAllBreedFilters($request->all());
		$posts = $service->getIndexPosts();
		$categories = $service->getIndexCategories();
		$popular = $service->getPopularBreeds();

		return view('pages.breeds.breeds', compact('breeds', 'filters', 'categories', 'popular', 'posts'));
	}

	public function category(Request $request)
	{
		$categories = CategoryUrls::getUrls();

		foreach ($categories as $category) {
			if($category['slug'] == $request->segment(2)) {
				$service = new PrismicService();

				$request->request->add([$category['filter_type'] => $category['category_id']]);
				$breeds = $service->getBreedsByFilter($request->all());

				$categories = $service->getIndexCategories();
				$popular = $service->getPopularBreeds();
				$currCategory = $service->getCategory($category['category_id']);

				break;
			}
		}

		return view('pages.category.category', compact('breeds', 'categories', 'popular', 'currCategory'));
	}

	public function searchBreed(Request $request)
	{
		$service = new PrismicService();
		return $service->searchBreed($request->input('query'));
	}

	public function searchBreedIndexFourColumns(Request $request)
	{
		$service = new PrismicService();
		return $service->searchBreedIndexFourColumns($request->input('query'));
	}

	public function blogPost(Request $request, $uri)
	{
		$service = new PrismicService();

		$data = $service->getBlogPost($uri);
		$categories = $service->getIndexCategories();
		$popular = $service->getPopularBreeds();

		return view('pages.breed_item.blog_post', compact('data', 'categories', 'popular'));
	}

	public function contacts(Request $request)
	{
		$service = new PrismicService();
		$categories = $service->getIndexCategories();
		$popular = $service->getPopularBreeds();

		return view('pages.contacts.contacts', compact('categories', 'popular'));
	}

	public function privacy_policy(Request $request)
	{
		$service = new PrismicService();
		$categories = $service->getIndexCategories();
		$popular = $service->getPopularBreeds();

		return view('pages.privacy_policy.privacy_policy', compact('categories', 'popular'));
	}

	public function agreement(Request $request)
	{
		$service = new PrismicService();
		$categories = $service->getIndexCategories();
		$popular = $service->getPopularBreeds();

		return view('pages.agreement.agreement', compact('categories', 'popular'));
	}
}