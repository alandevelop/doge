<?php


namespace App\Services;


use Prismic\Api;
use Prismic\Predicates;
use Illuminate\Support\Facades\Cache;

class PrismicService
{
	protected $api;

	protected $cacheExpiration = 864000; // 10 days

	public $filters;

	public function __construct()
	{
		$url = "https://doge.cdn.prismic.io/api/v2";
		$token = config('app.prismic_token');

		$api = Api::get($url, $token);

		$this->api = $api;

		$this->checkMasterRefStatus();

		$this->filters = [
			'cat_purpose' 		=> ['display_name_as' => 'Категории', 'field_uid_in_breed' => 'breed.cat_purposes.cat_purpose'],
			'cat_features' 		=> ['display_name_as' => 'Основные черты', 'field_uid_in_breed' => 'breed.cat_features.cat_feature'],
			'cat_size' 			=> ['display_name_as' => 'Размер', 'field_uid_in_breed' => 'breed.cat_size'],
			'cat_housing' 		=> ['display_name_as' => 'Тип жилья', 'field_uid_in_breed' => 'breed.cat_housing_type.cat_housing'],
			'cat_area_size' 	=> ['display_name_as' => 'Площадь жилья', 'field_uid_in_breed' => 'breed.cat_area_sizes.cat_area_size'],
			'cat_experience' 	=> ['display_name_as' => 'Опыт содержания', 'field_uid_in_breed' => 'breed.cat_experience'],
		];
	}

	protected function checkMasterRefStatus()
	{
		$api__master_ref = $this->api->refs()['Master']->getRef();

		$cached_master_ref = Cache::remember('prismic_master_ref', $this->cacheExpiration, function () use ($api__master_ref){
			return $api__master_ref;
		});

		if ($api__master_ref !== $cached_master_ref) {
			Cache::flush();
			Cache::put('prismic_master_ref', $api__master_ref, $this->cacheExpiration);
		}
	}

	public function getAllBreedsIndex()
	{
		$allBreeds = $this->getAllBreeds();

		$unsortedArr = [];
		foreach ($allBreeds as $letter) {
			if(count($unsortedArr) > 31) break;
			foreach ($letter as $breed) {
				$unsortedArr[] = $breed;
			}
		}

		$perColumn = (integer) ceil(count($unsortedArr) / 4);
		return array_chunk($unsortedArr, $perColumn);
	}

	public function getAllBreeds() :array
	{
		return Cache::remember('prismic_all_breeds', $this->cacheExpiration, function () {
			$breeds = [];

			$response = $this->api->query(
				Predicates::at('document.type', 'breed' ),
				[ 'fetch' => 'breed.breed_name, breed.first_letter, breed.is_active', 'pageSize' => 100, 'page' => 1]
			);

			foreach ($response->results as $result) {
				$breeds[] = [
					'uid'=> $result->uid,
					'name'=>$result->data->breed_name,
					'letter'=> $result->data->first_letter,
					'is_active' => $result->data->is_active
				];
			}

			if($response->total_pages > 1) {
				for ($i=2; $i<=$response->total_pages; $i++) {
					$response = $this->api->query(
						Predicates::at('document.type', 'breed' ),
						[ 'fetch' => 'breed.breed_name, breed.first_letter, breed.is_active', 'pageSize' => 100, 'page' => $i]
					);
					foreach ($response->results as $result) {
						$breeds[] = [
							'uid'=> $result->uid,
							'name'=>$result->data->breed_name,
							'letter'=> $result->data->first_letter,
							'is_active' => $result->data->is_active
						];
					}
				}
			}

			return $breeds ? $this->sortBreedsByLetters($breeds) : $breeds;
		});
	}

	public function getAllBreedFilters($selectedFilters) :array
	{
		$response = Cache::remember('prismic_all_breed_filters', $this->cacheExpiration, function () use ($selectedFilters) {
			return $this->api->query(
				Predicates::any('document.type',  array_keys($this->filters) ),
				['pageSize' => 100]
			);
		});

		foreach ($response->results as $result) {
			$isSelected = array_search($result->id, $selectedFilters) ? true : false;

			if($isSelected) {
				$copySelectedArr = $selectedFilters;
				unset($copySelectedArr[$result->type]);

				$excludeQueryStr = '';
				foreach ($copySelectedArr as $filtType => $filtVal) {
					$excludeQueryStr .= "&$filtType=$filtVal";
				}
				$excludeQueryStr = trim($excludeQueryStr, '&=');
			} else {
				$excludeQueryStr = '';
			}

			$this->filters[$result->type]['items'][] = [
				'name' => $result->data->name,
				'id' => $result->id,
				'selected' => $isSelected,
				'excludeQueryStr' => $excludeQueryStr
			];
		}

		return $this->filters;
	}

	public function getBreedsByFilter(array $GETParams) :array
	{
		$queryParams = [
			Predicates::at('document.type', 'breed'),
		];

		foreach($this->filters as $type => $filter) {
			if(isset($GETParams[$type]) && $GETParams[$type]) {
				$field = $filter['field_uid_in_breed'];
				$queryParams[] = Predicates::at("my.$field",  $GETParams[$type]);
			}
		}

		try {
			$response = $this->api->query($queryParams, ['pageSize' => 100, 'fetch' => 'breed.breed_name, breed.first_letter, breed.main_image, breed.is_active']);

			$arr = [];
			foreach ($response->results as $result) {
				$arr[] = [
					'uid' => $result->uid,
					'name' => $result->data->breed_name,
					'letter' => $result->data->first_letter,
					'main_image' => $result->data->main_image->url,
					'main_image_alt' => $result->data->main_image->alt,
					'is_active' => $result->data->is_active,
				];
			}

			return  $this->sortBreedsByLetters($arr);
		} catch (\Prismic\Exception\RequestFailureException $e) {
			return [];
		}
	}

	protected function sortBreedsByLetters(array $breeds) :array
	{
		$arr = [];
		foreach ($breeds as $breed) {
			$arr[$breed['letter']][] = $breed;
		}
		ksort($arr);

		return $arr;
	}

	public function searchBreed(string $query) :array
	{
		$response = $this->api->query(
			Predicates::fulltext('my.breed.search_queries', $query),
			['fetch' => 'breed.breed_name, breed.is_active']
		);

		if(!$response->results) return [];

		$arr = [];
		foreach ($response->results as $result) {
			$arr[] = [
				'uid' => $result->uid,
				'name' => $result->data->breed_name,
				'is_active' => $result->data->is_active
			];
		}

		return $arr;
	}

	public function searchBreedIndexFourColumns(string $query) :array
	{
		$arr = $this->searchBreed($query);

		if($arr) {
			$perColumn = (integer) ceil(count($arr) / 4);
			return array_chunk($arr, $perColumn);
		} else {
			return [];
		}
	}

	public function getIndexCategories() :array
	{
		return Cache::remember('prismic_index_categories', $this->cacheExpiration, function () {
			$response = $this->api->query(Predicates::any('document.type', ['cat_size', 'cat_purpose']));

			$arr = [];
			foreach ($response->results as $result) {
				$arr[$result->type][] = $result;
			}

			return $arr;
		});
	}

	public function getPopularBreeds()
	{
		return Cache::remember('prismic_popular_breeds', $this->cacheExpiration, function () {
			$response = $this->api->query(
				[
					Predicates::at('my.breed.is_pop', 1),
					Predicates::at('my.breed.is_active', 1.0),
				],
				[
				    'fetchLinks' => 'cat_purpose.name, cat_size.name',
                    'pageSize' => 60,
                ]
			);

			foreach($response->results as $result) {
				$cat_purposes = [];

				foreach ($result->data->cat_purposes as $item) {
					if(isset($item->cat_purpose->data)) {
						$cat_purposes[] = $item->cat_purpose->data->name;
					} else {
						continue;
					}
				}

				$result->data->cat_purposes = $cat_purposes;
				$result->data->cat_size = isset($result->data->cat_size->data) ? $result->data->cat_size->data->name : null;
			}

			return $response->results;
		});
	}

	public function getIndexPosts()
	{
		return Cache::remember('prismic_index_posts', $this->cacheExpiration, function () {
			$response = $this->api->query(
				Predicates::at('my.blog_post.on_index', 1)
			);

			foreach ($response->results as $result) {
				$result->data->pub_date_human = russianDate($result->first_publication_date);
			}

			return $response->results;
		});
	}

	public function getBreed(string $uri)
	{
		return Cache::remember("prismic_breed_$uri", $this->cacheExpiration, function () use ($uri){
			$response = $this->api->query(
				Predicates::at('my.breed.uid', $uri),
				[ 'fetchLinks' => 'cat_purpose.name, cat_size.name, breed.breed_name, breed.cat_purposes, breed.cat_size, breed.main_image' ]
			);

			if(!$response->results) return abort(404);

			$data = $response->results[0]->data;

			$cat_purposes = [];
			foreach ($data->cat_purposes as $item) {
				if(isset($item->cat_purpose->data)) {
					$cat_purposes[] = $item->cat_purpose->data->name;
				} else {
					continue;
				}
			}
			$data->cat_purposes = $cat_purposes;
			$data->cat_size = isset($data->cat_size->data) ? $data->cat_size->data->name : null;

			$similar = [];
			if($data->similar && isset($data->similar[0]->breed->data)) {
				foreach ($data->similar as $item) {
					$innerData = $item->breed->data;

					$tempArr = [
						'uid' => $item->breed->uid,
						'name' => $innerData->breed_name,
						'main_image' => $innerData->main_image->url,
						'image_alt'	=> $innerData->main_image->alt,
						'cat_size' => $innerData->cat_size->data->name,
						'cat_purposes' => $innerData->cat_purposes[0]->cat_purpose->data->name
					];
					$similar[] = $tempArr;
				}

				$data->similar = $similar;
			}

			return $data;
		});
	}

	public function getBlogPost(string $uri)
	{
		return Cache::remember("prismic_blog_post_$uri", $this->cacheExpiration, function () use ($uri){
			$response = $this->api->query(
				Predicates::at('my.blog_post.uid', $uri)
			);

			$post = $response->results[0]->data;
			$post->pub_date_human = russianDate($response->results[0]->first_publication_date);

			return $post;
		});
	}

	public function getAllPosts()
	{
		return Cache::remember('prismic_all_posts', $this->cacheExpiration, function () {
			$response = $this->api->query(
				Predicates::at('document.type', 'blog_post' ),
				[ 'pageSize' => 100, 'page' => 1]
			);

			return $response->results;
		});
	}

	public function getCategory(string $categoryId)
	{
		return $this->api->query(
			Predicates::at('document.id', $categoryId)
		)->results[0];
	}
}
