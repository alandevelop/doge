<?php


namespace App\Services;


class CategoryUrls
{
	public static function getUrls() :array
	{
		return [
			['slug'=>'ruchnye', 'category_id'=>'Xo3SlxAAACQAJnOb', 'filter_type' => 'cat_size'],
			['slug'=>'malenkie', 'category_id'=>'Xo3StxAAACEAJnQv', 'filter_type' => 'cat_size'],
			['slug'=>'srednie', 'category_id'=>'Xo3S1BAAAB8AJnS1', 'filter_type' => 'cat_size'],
			['slug'=>'krupnye', 'category_id'=>'Xo3S8hAAAB8AJnVC', 'filter_type' => 'cat_size'],
			['slug'=>'ochen-krupnye', 'category_id'=>'Xo3TBxAAAB8AJnWo', 'filter_type' => 'cat_size'],

			['slug'=>'boycovskie', 'category_id'=>'Xo3P8RAAACEAJmdu', 'filter_type' => 'cat_purpose'],
			['slug'=>'dekorativnye', 'category_id'=>'Xo3P4RAAACQAJmcb', 'filter_type' => 'cat_purpose'],
			['slug'=>'kompanony', 'category_id'=>'Xo3P0BAAACEAJma-', 'filter_type' => 'cat_purpose'],
			['slug'=>'ohotnichi', 'category_id'=>'Xo3PfhAAAB8AJmU5', 'filter_type' => 'cat_purpose'],
			['slug'=>'pastushi', 'category_id'=>'Xo3QBBAAACIAJmfI', 'filter_type' => 'cat_purpose'],
			['slug'=>'storozhevye', 'category_id'=>'XrF7phAAACEAj4A6', 'filter_type' => 'cat_purpose'],
			['slug'=>'sluzhebnye', 'category_id'=>'Xo3PWRAAACIAJmSE', 'filter_type' => 'cat_purpose'],
		];
	}
}