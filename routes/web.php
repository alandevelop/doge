<?php

use Illuminate\Support\Facades\Route;

use App\Services\CategoryUrls;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');

foreach (CategoryUrls::getUrls() as $category) {
	$slug = $category['slug'];
	Route::get("/poroda/$slug", 'MainController@category');
}
Route::get('/poroda/{uri}', 'MainController@breed')->name('breed');
Route::get('/poroda/', 'MainController@breeds')->name('breeds');

Route::redirect('/breed/{uri}', '/poroda/{uri}', 301);
Route::redirect('/breed/', '/poroda', 301);
Route::redirect('/breeds/', '/poroda', 301);

Route::get('/search-breed', 'MainController@searchBreed');
Route::get('/search-breed-column', 'MainController@searchBreedIndexFourColumns');
Route::get('/blog/{uri}', 'MainController@blogPost')->name('post');
Route::get('/contacts/', 'MainController@contacts')->name('contacts');
Route::get('/privacy_policy/', 'MainController@privacy_policy')->name('privacy_policy');
Route::get('/agreement/', 'MainController@agreement')->name('agreement');
