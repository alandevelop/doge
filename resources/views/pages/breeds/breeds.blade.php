<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="path/to/image.jpg">
    <meta name="theme-color" content="#000">
    <link rel="icon" href="/favicon.ico?q=1723">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon-180x180.png">
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/main.css">

    <title>Все породы собак по алфавиту - от А до Я всего +450 пород</title>
    <meta name="description" content="Все породы собак по алфавиту, которые существуют в Мире - всего +450 пород с фотографиями, названиями, и подробным описанием и обзором,  отзывами владельцев">

    @include('components.headAndBodyEnd.head')
</head>
<body>

@include('components.header', ['index'=>false])

<section class="breeds-page">
    @include('components.top_line', [
	    'title' => 'Все породы собак по алфавиту',
	    'breadcrumbs' => [
	    	['anchor' => 'Породы собак', 'href'=> route('breeds')]
        ]
    ])
    <div class="breeds-main">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="breeds-main__title">Выберите породу из списка или используйте фильтр</h2>
                    <form class="search">
                        <div class="input-wrap">
                            <input type="text" name="search" data-name="search" placeholder="Быстрый поиск породы">
                            <button class="search__icon-wrap">
                                <svg class="icon icon--search">
                                    <use xlink:href="/img/svg-sprite.svg#search"></use>
                                </svg>
                            </button>
                        </div>
                        <ul class="search__list" style="display: none;">
                            <li class="search__item"><a class="search__link" href="single.html">Бульдог</a></li>
                            <li class="search__item"><a class="search__link" href="single.html">Бульдог1</a></li>
                            <li class="search__item"><a class="search__link" href="single.html">Бульдог2</a></li>
                            <li class="search__item"><a class="search__link" href="single.html">Бульдог3</a></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <form class="breeds-filter" action="{{route('breeds')}}">
        <div class="container">
            <div class="row">
                @foreach($filters as $type => $filter)
                    <div class="col-6 col-lg-3 breeds-filter__col">
                        <div class="breeds-filter__title">{{$filter['display_name_as']}}</div>
                        <select class="breeds-filter__select" name="{{$type}}">
                            <option value="">Выбрать</option>
                            @foreach($filter['items'] as $item)
                                <option @if($item['selected']) selected @endif value="{{$item['id']}}">{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                @endforeach
                <div class="col-6 col-lg-3 breeds-filter__col"><button type="submit" class="breeds-filter__button btn">Применить</button></div>
            </div>
        </div>
    </form>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breeds-letter">
                    <div class="breeds-letter__title">Породы по буквам</div>
                    <ul class="breeds-letter__list">
                        @foreach($breeds as $key => $value)
                            <li class="breeds-letter__item"><a class="breeds-letter__link" href="#" data-href="{{$key}}">{{$key}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>

            @php $hasSelected = false; @endphp
            @foreach($filters as $type => $filter)
                @foreach($filter['items'] as $item)
                    @if($item['selected']) @php $hasSelected = true; @endphp @endif
                @endforeach
            @endforeach

            @if($hasSelected)
                <div class="col-12">
                    <div class="breeds-filter-result">
                        <div class="breeds-filter-result__title">Результаты подбора породы</div>
                        <div class="breeds-filter-result__bottom-wrap">
                            <div class="breeds-filter-result__list">
                                @foreach($filters as $type => $filter)
                                    @foreach($filter['items'] as $item)
                                        @if($item['selected'])
                                            <a href="{{route('breeds')}}?{{$item['excludeQueryStr']}}" class="breeds-filter-result__item">{{$item['name']}}
                                                <div class="breeds-filter-result__icon-wrap">
                                                    <svg class="icon icon--close"><use xlink:href="/img/svg-sprite.svg#close"></use></svg>
                                                </div>
                                            </a>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                            <a class="breeds-filter-result__remove-btn" href="{{route('breeds')}}">Очистить</a>
                        </div>
                    </div>
                </div>
            @endif

            <div class="col-12">
                <div class="breeds-letter-content">
                    <ul class="breeds-letter-content__parent">
                        @foreach($breeds as $key => $value)
                            <li class="breeds-letter-content__parent-item" data-id="{{$key}}">
                                <div class="breeds-letter-content__title-wrap">
                                    <div class="breeds-letter-content__title">{{$key}}</div>
                                    @if($loop->first)
                                        <button class="breeds-letter-content__remove-btn" style="display: none;">Очистить</button>
                                    @endif
                                </div>
                                <ul class="breeds-letter-content__list">
                                    @foreach($value as $breed)
                                        <li class="breeds-letter-content__item">
                                            <a class="breeds-letter-content__link"
                                               @if($breed['is_active']) href="{{route('breed', ['uri'=>$breed['uid']])}}" @endif
                                            >{{$breed['name']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


@include('components.articles')
@include('components.footer')
@include('components.request_popup')
@include('components.success_popup')

<!-- Scripts-->
<script src="/js/libs.min.js"></script>
<script src="/js/main.min.js"></script>
<script src="/js/custom.js"></script>

@include('components.headAndBodyEnd.bodyEnd')
</body>
</html>