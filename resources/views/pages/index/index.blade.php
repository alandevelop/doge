<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="path/to/image.jpg">
    <meta name="theme-color" content="#000">
    <link rel="icon" href="/favicon.ico?q=1723">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <title>Породы собак с фото и названиями - всего 500+ пород</title>

    <meta name="description" content="Породы собак с фото и названиями - каталог всех пород собак от А до Я, подробное описание породы, корма, вязки, питания, дрессировка, видео и фото собачек и псов">

    @include('components.headAndBodyEnd.head')
</head>
<body>

@include('components.header', ['index'=>true])


<section class="main" style="background: url('/img/bg/main-bg.webp') no-repeat center center/cover">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="main__title">Все породы собак с фото и названиями</h1>
                <form class="search">
                    <div class="input-wrap">
                        <input type="text" name="search" data-name="search" placeholder="Введите породу собаки">
                        <button class="search__icon-wrap">
                            <svg class="icon icon--search">
                                <use xlink:href="/img/svg-sprite.svg#search"></use>
                            </svg>
                        </button>
                        <div class="custom_loading" style="display: none; position: absolute; top: 28%; left: 1.375rem;">
                            <img src="/img/loading.svg" alt="loading">
                        </div>
                    </div>
                    <ul class="search__list search__list--disabled">
                    </ul>
                </form>
                <div class="main__desc">
                    <p>Более 500 пород собак с подробным описанием, привычками, повадками и правилами ухода</p>
                </div>
            </div>
        </div>
        <div class="benefits">
            <div class="row benefits__row">
                <div class="col-6 col-lg-3 benefits__col">
                    <div class="benefits__item">
                        <div class="benefits__icon-wrap"><svg class="icon icon--dog">
                                <use xlink:href="/img/svg-sprite.svg#dog"></use>
                            </svg></div>
                        <div class="benefits__text">Все 580 известных порода собак</div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 benefits__col">
                    <div class="benefits__item">
                        <div class="benefits__icon-wrap"><svg class="icon icon--binocular">
                                <use xlink:href="/img/svg-sprite.svg#binocular"></use>
                            </svg></div>
                        <div class="benefits__text">Побор и сравнение корма для собак</div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 benefits__col">
                    <div class="benefits__item">
                        <div class="benefits__icon-wrap"><svg class="icon icon--man-user">
                                <use xlink:href="/img/svg-sprite.svg#man-user"></use>
                            </svg></div>
                        <div class="benefits__text">17 582 пользователя на платформе</div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 benefits__col">
                    <div class="benefits__item">
                        <div class="benefits__icon-wrap"><svg class="icon icon--check">
                                <use xlink:href="/img/svg-sprite.svg#check"></use>
                            </svg></div>
                        <div class="benefits__text">Одобрено эспертами по собаковедению</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dog-category">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="dog-category__title">Категории собак по назначению</h2>
            </div>
        </div>
        <div class="row dog-category__row">

            @foreach($categories['cat_purpose'] as $category)
{{--                @php--}}
{{--                    $type = $category->type;--}}
{{--                    $id = $category->id;--}}
{{--                @endphp--}}

                <div class="dog-category__col col-6 col-md-4 col-lg-2">
                    <div class="dog-category__item-wrap">
                        <a class="dog-category__item" href="/poroda/{{$category->uid}}">
{{--                        <a class="dog-category__item" href="{{route('breeds')}}?{{$type}}={{$id}}">--}}
{{--                            <div class="dog-category__count">178</div>--}}
                            <div class="dog-category__icon-wrap">
                                {!! $category->data->icon !!}
                            </div>
                        </a>
                        <div class="dog-category__item-title">{{$category->data->name}}</div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="dog-category__title">Категории собак по размеру</h2>
            </div>
        </div>
        <div class="row">
            <div class="dog-category__col col-6 col-md-4 col-lg-2">
                <div class="dog-category__item-wrap">
                    <a class="dog-category__item" href="{{route('breeds')}}">
{{--                        <div class="dog-category__count">546</div>--}}
                        <div class="dog-category__icon-wrap">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 51.21">
                                <g data-name="Слой 2">
                                    <g data-name="Слой 1">
                                        <path class="cls-1" d="M42.25,34.22a27.55,27.55,0,0,1-5.48-8.3,9.36,9.36,0,0,0-17.28,0,26.39,26.39,0,0,1-5.86,8.66h0a9.4,9.4,0,0,0-3.12,7,9.29,9.29,0,0,0,4.23,7.87,10.37,10.37,0,0,0,8.51,1.37,19.75,19.75,0,0,1,9.82,0,10.31,10.31,0,0,0,8.47-1.33,9.42,9.42,0,0,0,.72-15.28ZM40.32,47.68a8.15,8.15,0,0,1-6.67,1A20.75,20.75,0,0,0,28.11,48a21,21,0,0,0-5.45.68A8.16,8.16,0,0,1,16,47.62a7.22,7.22,0,0,1-.88-11.39,28.2,28.2,0,0,0,6.43-9.44v0a7.18,7.18,0,0,1,13.26,0v0a29.82,29.82,0,0,0,6,9l.1.08a7.24,7.24,0,0,1-.53,11.78Z" />
                                        <path class="cls-1" d="M24.46,14.94a11.19,11.19,0,0,0,1.2-6.7C25.1,3.54,21.86-.4,18.23,0a5.62,5.62,0,0,0-4.1,3,11.15,11.15,0,0,0-1.2,6.69C13.84,17.44,21,21,24.46,14.94ZM16,4.15a3.37,3.37,0,0,1,2.74-2c2.15,0,4.31,2.89,4.72,6.31a8.94,8.94,0,0,1-.93,5.35c-2.52,4.4-6.86.69-7.46-4.35A8.91,8.91,0,0,1,16,4.15Z" />
                                        <path class="cls-1" d="M14,26.7a9.94,9.94,0,0,0-1.08-6.07,10,10,0,0,0-4.33-4.4,5.93,5.93,0,0,0-5.43-.13,5.91,5.91,0,0,0-3,4.51,10,10,0,0,0,1.08,6.07,9.94,9.94,0,0,0,4.33,4.39,5.92,5.92,0,0,0,5.43.14A6,6,0,0,0,14,26.7Zm-4,2.56a3.8,3.8,0,0,1-3.46-.15,7.8,7.8,0,0,1-3.35-3.44A7.73,7.73,0,0,1,2.27,21,3.82,3.82,0,0,1,4.14,18a3.84,3.84,0,0,1,3.47.16A7.71,7.71,0,0,1,11,21.63a7.78,7.78,0,0,1,.87,4.72A3.79,3.79,0,0,1,9.94,29.26Z" />
                                        <path class="cls-1" d="M36.2,18c3.32,0,6.35-3.62,6.89-8.24C43.68,4.85,41.2,0,37.26,0c-3.32,0-6.35,3.62-6.89,8.24C29.78,13.2,32.27,18,36.2,18ZM32.54,8.52c.4-3.42,2.56-6.31,4.72-6.31l.26,0a3.66,3.66,0,0,1,2.6,2.19,9,9,0,0,1,.8,5.1c-.42,3.54-2.77,6.58-5,6.29h0a3.63,3.63,0,0,1-2.58-2.19A9,9,0,0,1,32.54,8.52Z" />
                                        <path class="cls-1" d="M55.89,20.61c-.69-4.26-4.54-6.3-8.46-4.37A10.42,10.42,0,0,0,42,26.7c.7,4.32,4.59,6.28,8.46,4.38A10.42,10.42,0,0,0,55.89,20.61Zm-6.37,8.5a3.47,3.47,0,0,1-5.34-2.76,8.24,8.24,0,0,1,4.21-8.15c2.58-1.26,4.88-.07,5.34,2.76A8.24,8.24,0,0,1,49.52,29.11Z" />
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </a>
                    <div class="dog-category__item-title">Все собаки</div>
                </div>
            </div>
            @foreach($categories['cat_size'] as $category)
{{--                @php--}}
{{--                    $type = $category->type;--}}
{{--                    $id = $category->id;--}}
{{--                @endphp--}}

                <div class="col-6 col-md-4 col-lg-2 dog-category__col">
                    <div class="dog-category__item-wrap">
                        <a class="dog-category__item" href="/poroda/{{$category->uid}}">
{{--                            <div class="dog-category__count">546</div>--}}
                            <div class="dog-category__icon-wrap">
                                <svg class="icon icon--lapkins">
                                    {!! $category->data->icon !!}
                                </svg>
                            </div>
                        </a>
                        <div class="dog-category__item-title">{{$category->data->name}}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section class="popular-breeds">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="popular-breeds__title">Популярные породы собак</h2>
            </div>
        </div>
        <div class="row">
            @foreach($popular as $item)
                <div class="col-6 col-md-4 col-lg-3 popular-breeds__col">
                    <div class="popular-breeds__item-wrap">
                        <a class="popular-breeds__item" href="{{route('breed', ['uri'=>$item->uid])}}">
                            <div class="popular-breeds__img-wrap">
                                <img class="popular-breeds__img" src="{{$item->data->main_image->url}}" alt="{{$item->data->main_image->alt}}" />
                            </div>
                            <div class="popular-breeds__item-title">{{$item->data->breed_name}}</div>
                        </a>
                        <ul class="tags">
                            <li class="tags__item">{{mb_strtoupper($item->data->cat_size)}}</li>
                            <li class="tags__item">{{mb_strtoupper($item->data->cat_purposes[0])}}</li>
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section class="all-breeds">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="all-breeds__top-line">
                    <h2 class="all-breeds__title">Каталог всех пород от А до Я</h2>
                    <form class="breeds-search">
                        <div class="input-wrap">
                            <input type="text" name="breeds-search" data-name="breeds-search" placeholder="Введите название породы">
                            <button class="breeds-search__icon-wrap">
                                <svg class="icon icon--search">
                                    <use xlink:href="/img/svg-sprite.svg#search"></use>
                                </svg>
                            </button>
                            <div class="custom_loading" style="display: none; position: absolute; top: 22%; left: 1.375rem;">
                                <img src="/img/loading.svg" alt="loading">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="breeds">
            <div class="row indexColumnsSearchResults">
                @foreach($allBreedsIndex as $column)
                    <div class="col-12 col-sm-6 col-lg-3 breeds__col">
                        <ul class="breeds__list breeds__list--letter-a">
                            @foreach($column as $breed)
                                <li class="breeds__item">
                                    <a class="breeds__link"
                                       @if($breed['is_active']) href="{{route('breed', ['uri'=>$breed['uid']])}}" @endif
                                    >{{$breed['name']}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12">
                    <a class="more-link" href="{{route('breeds')}}">
                        <div class="more-link__text">Смотреть все породы собак</div>
                        <div class="more-link__icon-wrap">
                            <svg class="icon icon--arrow">
                                <use xlink:href="/img/svg-sprite.svg#arrow"></use>
                            </svg>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('components.articles')

@include('components.footer')
@include('components.request_popup')
@include('components.success_popup')

<!-- Scripts-->
<script src="/js/libs.min.js"></script>
<script src="/js/main.min.js"></script>
<script src="/js/custom.js"></script>

@include('components.headAndBodyEnd.bodyEnd')
</body>
</html>