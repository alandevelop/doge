<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="path/to/image.jpg">
    <meta name="theme-color" content="#000">
    <link rel="icon" href="/favicon.ico?q=1723">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon-180x180.png">
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <title>{{$currCategory->data->seo_title}}</title>
    <meta name="description" content="{{$currCategory->data->seo_description}}">

    @include('components.headAndBodyEnd.head')
</head>
<body>

@include('components.header', ['index'=>false])

<section class="breeds-page">
    @include('components.top_line', [
	    'title' => $currCategory->data->h1,
	    'breadcrumbs' => [
	    	['anchor' => $currCategory->data->h1]
        ]
    ])
    <div class="container">

        <section class="popular-breeds">
            <div class="row">
                @foreach($breeds as $letter => $letterBreeds)
                    @foreach($letterBreeds as $breed)
                        <div class="col-6 col-md-4 col-lg-3 popular-breeds__col">
                            <div class="popular-breeds__item-wrap">
                                <a class="popular-breeds__item"
                                   @if($breed['is_active']) href="{{route('breed', ['uri'=>$breed['uid']])}}" @endif
                                >
                                    <div class="popular-breeds__img-wrap">
                                        <img class="popular-breeds__img" src="{{$breed['main_image']}}" alt="{{$breed['main_image_alt']}}" />
                                    </div>
                                    <div class="popular-breeds__item-title">{{$breed['name']}}</div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </section>

        <div class="row" style="margin-bottom: 40px;">
            <div class="col-12">
                <div class="breeds-letter">
                    <div class="breeds-filter-result__title">{{$currCategory->data->h2}}</div>
                    <ul class="breeds-letter__list">
                        @foreach($breeds as $key => $value)
                            <li class="breeds-letter__item">
                                <a class="breeds-letter__link" href="#" data-href="{{$key}}">{{$key}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="col-12">
                <div class="breeds-letter-content">
                    <ul class="breeds-letter-content__parent">
                        @foreach($breeds as $key => $value)
                            <li class="breeds-letter-content__parent-item" data-id="{{$key}}">
                                <div class="breeds-letter-content__title-wrap">
                                    <div class="breeds-letter-content__title">{{$key}}</div>
                                    @if($loop->first)
                                        <button class="breeds-letter-content__remove-btn" style="display: none;">Очистить</button>
                                    @endif
                                </div>
                                <ul class="breeds-letter-content__list">
                                    @foreach($value as $breed)
                                        <li class="breeds-letter-content__item">
                                            <a class="breeds-letter-content__link"
                                               @if($breed['is_active'])  href="{{route('breed', ['uri'=>$breed['uid']])}}" @endif
                                            >{{$breed['name']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


@include('components.footer')
@include('components.request_popup')
@include('components.success_popup')

<!-- Scripts-->
<script src="/js/libs.min.js"></script>
<script src="/js/main.min.js"></script>
<script src="/js/custom.js"></script>

@include('components.headAndBodyEnd.bodyEnd')
</body>
</html>