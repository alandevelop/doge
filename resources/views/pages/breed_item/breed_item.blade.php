<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="path/to/image.jpg">
    <meta name="theme-color" content="#000">

    <link rel="icon" href="/favicon.ico?q=1723">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/custom.css{{assets_cache_key()}}">

    <title>{{$data->seo_title}}</title>
    <meta name="description" content="{{$data->seo_description}}">
    <meta name="Keywords" content="{{$data->seo_keywords}}">

    @include('components.headAndBodyEnd.head')

    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?168"></script>
</head>
<body>

@include('components.header', ['index'=>false])

@include('pages.breed_item.components.main_block')


<section class="single">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 single__col-left" >
                <div style="max-width: 628px; width: 100%;">
                    @foreach($data->body as $section)
                        @switch($section->slice_type)
                            @case('paragraph')
                            @include('pages.breed_item.components.paragraph')
                            @break

                            @case('ol_rounded')
                            @include('pages.breed_item.components.ol_rounded')
                            @break

                            @case('block_icon_right')
                            @include('pages.breed_item.components.block_icon_right')
                            @break

                            @case('ul_dot')
                            @include('pages.breed_item.components.ul_dot')
                            @break

                            @case('ol_frameless')
                            @include('pages.breed_item.components.ol_frameless')
                            @break

                            @case('ol_standart')
                            @include('pages.breed_item.components.ol_standart')
                            @break

                            @case('ul_paw_check')
                            @include('pages.breed_item.components.ul_check')
                            @break

                            @case('fact_2')
                            @include('pages.breed_item.components.fact_2')
                            @break

                            @case('fact_1')
                            @include('pages.breed_item.components.fact_1')
                            @break

                            @case('block_icon_left')
                            @include('pages.breed_item.components.block_icon_left')
                            @break

                            @case('image')
                            <img class="single__img" src="{{$section->primary->image->url}}" alt="{{$section->primary->image->alt}}" style="margin: 10px 0 33px;">
                            @break

                            @case('feature')
                            @include('pages.breed_item.components.feature')
                            @break

                            @case('title_h3')
                            <h3 class="single__small-title"  @if($section->primary->html_id) id="{{$section->primary->html_id}}"@endif style="margin: 42px 0 10px">{{$section->primary->text_title}}</h3>
                            @break

                            @case('ul_paw_grad')
                            @include('pages.breed_item.components.ul_paw_grad')
                            @break

                            @case('ul_paw')
                            @include('pages.breed_item.components.ul_paw')
                            @break

                            @case('title_h2')
                            <h2 class="single__title" @if($section->primary->html_id) id="{{$section->primary->html_id}}" @endif >{{$section->primary->text_title}}</h2>
                            @break

                            @case('similar')
                            @include('pages.breed_item.components.similar')
                            @break

                            @case('price')
                            @include('pages.breed_item.components.price')
                            @break

                        @endswitch
                    @endforeach

                    @include('pages.breed_item.components.gallery')
                </div>

                <div id="vk_comments"></div>

            </div>
            @include('pages.breed_item.components.contents')
        </div>
    </div>
</section>

@include('components.footer')
@include('components.request_popup')
@include('components.success_popup')

<!-- Scripts-->
<script src="/js/libs.min.js"></script>
<script src="/js/main.min.js"></script>

@include('components.headAndBodyEnd.bodyEnd')

<script type="text/javascript">
    VK.init({apiId: 7488223, onlyWidgets: true});
    VK.Widgets.Comments("vk_comments", {limit: 15, attach: "*"});
</script>
</body>
</html>