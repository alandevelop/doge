<div class="col-12 col-lg-4 single__col-right">
    <div class="content-nav">
        <h3 class="content-nav__title" style="margin-bottom: 0;">Содержание</h3>
        <div class="content-nav__wrap">
            @foreach($data->body as $section)
                @switch($section->slice_type)
                    @case('title_h2')
                        <a
                            style="margin-top: 17px;"
                            class="content-nav__list-heading"
                            @if($section->primary->html_id)href="#{{$section->primary->html_id}}"@endif
                        >{{$section->primary->title_in_contents}}</a>
                    @break

                    @case('title_h3')
                        <a
                            style="margin-bottom: 3px; line-height: 21px; display: block;"
                            class="content-nav__link"
                            @if($section->primary->html_id)href="#{{$section->primary->html_id}}"@endif
                        >{{$section->primary->title_in_contents}}</a>
                    @break
                @endswitch
            @endforeach
        </div>
    </div>
</div>