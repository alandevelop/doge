<div class="single__interesting-fact-wrap" style="margin: 16px 0;">
    <div class="interesting-fact interesting-fact--style-1">
        <div class="interesting-fact__title">{{$section->primary->title}}</div>
        <div class="interesting-fact__desc">
            <p style="white-space: pre-line">{{$section->primary->text}}</p>
        </div>
        <div class="interesting-fact__bg">
            <img class="interesting-fact__bg-img" src="/img/single/interesting-bg.png" alt="">
        </div>
    </div>
</div>