<div class="single__dog-list-wrap" style="margin-bottom: 22px;">
    <div class="dog-list dog-list--style-1">
        <ul class="dog-list__wrap">
            @foreach($section->items as $item)
                <li class="dog-list__item">
                    <div class="dog-list__icon-wrap"><svg class="icon icon--check-2">
                            <use xlink:href="/img/svg-sprite.svg#check-2"></use>
                        </svg></div>
                    <div class="dog-list__text">{{$item->ul_check_item}}</div>
                </li>
            @endforeach
        </ul>

        @isset($section->primary->image->url)
            <div class="dog-list__img-wrap">
                <img class="dog-list__img" src="{{$section->primary->image->url}}" alt="{{$section->primary->image->alt}}">
            </div>
        @endisset
    </div>
</div>