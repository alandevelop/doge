<div class="single__dog-list single__dog-list--style-3" style="margin-bottom: 22px;">
    <div class="dog-list dog-list--style-3">
        <ul class="dog-list__wrap">
            @foreach($section->items as $item)
                <li class="dog-list__item">
                    <div class="dog-list__desc">
                        <p style="white-space: pre-line">{{$item->ul_dot_item}}</p>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>