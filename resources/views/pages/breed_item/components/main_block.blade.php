<section class="single-main">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="single-main__mobile-container"></div>
                <div class="single-main__slider-wrap">
                    <div class="single-main__slider">
                        @foreach($data->gallery_group as $item)
                            <div class="single-main__slider-item"><img src="{{$item->gallery_item->big->url}}" alt="{{$item->gallery_item->alt}}"></div>
                        @endforeach

                        @foreach($data->video_group as $item)
                            <div class="single-main__slider-item">{!! $item->iframe_video !!}</div>
                        @endforeach
                    </div>
                    <div class="single-main__slider-nav">
                        @foreach($data->gallery_group as $item)
                            <div class="single-main__slider-nav-item"><img src="{{$item->gallery_item->preview->url}}" alt="{{$item->gallery_item->alt}}"></div>
                        @endforeach

                        @foreach($data->video_group as $item)
                            <div class="single-main__slider-nav-item single-main__slider-nav-item--video"><img src="{{$item->image_video->preview->url}}" alt="{{$item->image_video->alt}}"></div>
                        @endforeach
                    </div>
                    <div class="single-main__photo-count">Фото <span>{{$data->breed_name}}</span> (<span>{{count($data->gallery_group)}}</span> фото)</div>
                </div>
            </div>
            <div class="col-12 col-lg-6 single-main__right-col">
                <div class="single-main__top-line">
                    <nav class="bread-crumbs">

                        <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="bread-crumbs__list">
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="bread-crumbs__item">
                                <a itemprop="item" href="/" class="bread-crumbs__link">
                                    <span itemprop="name">Главная</span>
                                </a>
                                <meta itemprop="position" content="1" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="bread-crumbs__item">
                                <a itemprop="item" href="{{route('breeds')}}" class="bread-crumbs__link">
                                    <span itemprop="name">Породы собак</span>
                                </a>
                                <meta itemprop="position" content="2" />
                            </li>
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="bread-crumbs__item">
                                <a itemprop="item" class="bread-crumbs__link">
                                    <span itemprop="name">{{$data->breed_name}}</span>
                                </a>
                                <meta itemprop="position" content="3" />
                            </li>
                        </ul>

                    </nav>
                    <h1 class="single-main__title">{{$data->h1}}</h1>
{{--                    <div class="single-main__tags-wrap"><a class="info-tag" href="#">--}}
{{--                            <div class="info-tag__icon-wrap"><img class="info-tag__icon" src="/img/icons/review.svg" alt=""></div>--}}
{{--                            <div class="info-tag__count">120 <span>отзывов</span></div>--}}
{{--                        </a><a class="info-tag" href="#">--}}
{{--                            <div class="info-tag__icon-wrap"><img class="info-tag__icon" src="/img/icons/like.svg" alt=""></div>--}}
{{--                            <div class="info-tag__count">350 <span>лайков</span></div>--}}
{{--                        </a><a class="info-tag" href="#">--}}
{{--                            <div class="info-tag__icon-wrap"><img class="info-tag__icon" src="/img/icons/star.svg" alt=""></div>--}}
{{--                            <div class="info-tag__count">50 000 <span>наград</span></div>--}}
{{--                        </a><a class="info-tag" href="#">--}}
{{--                            <div class="info-tag__icon-wrap"><img class="info-tag__icon" src="/img/icons/movie-player.svg" alt=""></div>--}}
{{--                            <div class="info-tag__count">15 <span>фильмов</span></div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>
                <div class="single-main__info-wrap">
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--ruble">
                                <use xlink:href="/img/svg-sprite.svg#ruble"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Стоимость</div>
                            <div class="breeds-info-card__desc">{{$data->cost}}</div>
                        </div>
                    </a>
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--dog-head">
                                <use xlink:href="/img/svg-sprite.svg#dog-head"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Длительность жизни</div>
                            <div class="breeds-info-card__desc">{{$data->lifespan}}</div>
                        </div>
                    </a>
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--comb">
                                <use xlink:href="/img/svg-sprite.svg#comb"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Шерсть</div>
                            <div class="breeds-info-card__desc">{{$data->wool}}</div>
                        </div>
                    </a>
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--big-dog">
                                <use xlink:href="/img/svg-sprite.svg#big-dog"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Рождаемость</div>
                            <div class="breeds-info-card__desc">{{$data->birth_ratex}}</div>
                        </div>
                    </a>
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--ruler">
                                <use xlink:href="/img/svg-sprite.svg#ruler"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Рост в холке, см</div>
                            <div class="breeds-info-card__desc">{{$data->height}}</div>
                        </div>
                    </a>
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--weight">
                                <use xlink:href="/img/svg-sprite.svg#weight"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Вес, кг</div>
                            <div class="breeds-info-card__desc">{{$data->weight}}</div>
                        </div>
                    </a>
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--house">
                                <use xlink:href="/img/svg-sprite.svg#house"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Содержание</div>
                            <div class="breeds-info-card__desc">{{$data->keeping}}</div>
                        </div>
                    </a>
                    <a class="breeds-info-card">
                        <div class="breeds-info-card__icon-wrap"><svg class="icon icon--skill">
                                <use xlink:href="/img/svg-sprite.svg#skill"></use>
                            </svg></div>
                        <div class="breeds-info-card__right-col">
                            <div class="breeds-info-card__name">Назначение</div>
                            <div class="breeds-info-card__desc">@foreach($data->cat_purposes as $item) {{$item}}@if(!$loop->last), @endif @endforeach</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>