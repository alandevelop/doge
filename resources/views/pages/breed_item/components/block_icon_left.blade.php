<div class="single__distinctive-features single__distinctive-features--advantages" style="margin-bottom: 8px;">
    <div class="distinctive-features distinctive-features--advantages">
        <div class="distinctive-features__item">
            <div class="distinctive-features__title">{{$section->primary->title}}</div>
            <div class="distinctive-features__icon-wrap">
                <img class="distinctive-features__icon" src="{{$section->primary->icon->url}}" alt="{{$section->primary->icon->alt}}">
            </div>
            <div class="distinctive-features__desc">
                <p style="white-space: pre-line">{{$section->primary->text}}</p>
            </div>
        </div>
    </div>
</div>