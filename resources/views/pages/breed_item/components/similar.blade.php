<div class="similar-breeds" style="margin-bottom: 58px;">

    @foreach($data->similar as $item)
        <div class="similar-breeds__col" style="display: block;">
            <div class="popular-breeds__item-wrap"><a class="popular-breeds__item" href="{{route('breed', ['uri' => $item['uid']])}}">
                    <div class="popular-breeds__img-wrap">
                        <img class="popular-breeds__img" src="{{$item['main_image']}}" alt="{{$item['image_alt']}}" />
                    </div>
                    <div class="popular-breeds__item-title">{{$item['name']}}</div>
                </a>
                <ul class="tags">
                    <li class="tags__item">{{$item['cat_purposes']}}</li>
                    <li class="tags__item">{{$item['cat_size']}}</li>
                </ul>
            </div>
        </div>
    @endforeach



</div>