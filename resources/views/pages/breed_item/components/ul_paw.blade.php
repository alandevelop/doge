<div class="single__weight-list-wrap" style="margin-bottom: 17px;">
    @php
        $itemsPerColumn = ceil(count($section->items) / 2);
    @endphp

    <div class="weight-list">
        <div class="weight-list__left-col">
            @foreach($section->items as $item)
                @break($loop->iteration > $itemsPerColumn)
                <div class="weight-list__item">
                    <div class="weight-list__icon-wrap"><svg class="icon icon--paw">
                            <use xlink:href="/img/svg-sprite.svg#paw"></use>
                        </svg></div>
                    <div class="weight-list__title">{{$item->ul_paw_item}}</div>
                </div>
            @endforeach
        </div>

        <div class="weight-list__right-col">
            @foreach($section->items as $item)
                @continue($loop->iteration <= $itemsPerColumn)
                <div class="weight-list__item">
                    <div class="weight-list__icon-wrap"><svg class="icon icon--paw">
                            <use xlink:href="/img/svg-sprite.svg#paw"></use>
                        </svg></div>
                    <div class="weight-list__title">{{$item->ul_paw_item}}</div>
                </div>
            @endforeach
        </div>
    </div>
</div>