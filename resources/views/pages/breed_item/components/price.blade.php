<div class="single__dog-price-wrap">
    <div class="dog-price-card">

        @foreach($data->price as $item)
            <div class="dog-price-card__col">
                <div class="dog-price-card__top-line">
                    <div class="dog-price-card__img-wrap">
                        <img class="dog-price-card__img" src="{{$item->icon->url}}" alt="{{$item->icon->alt}}">
                    </div>
                    <div class="dog-price-card__price">{{$item->price_text}}</div>
                </div>
                <div class="dog-price-card__bottom-line">
                    <div class="dog-price-card__text">{{$item->type}}</div>
                </div>
            </div>
        @endforeach

    </div>
</div>