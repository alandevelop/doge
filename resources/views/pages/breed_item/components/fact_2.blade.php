<div class="single__interesting-fact-wrap single__interesting-fact-wrap--style-2" style="margin: 16px 0;">
    <div class="interesting-fact interesting-fact--style-2">
        <div class="interesting-fact__title">{{$section->primary->title}}</div>
        <div class="interesting-fact__desc">
            <p style="white-space: pre-line">{{$section->primary->text}}</p>
        </div>
    </div>
</div>