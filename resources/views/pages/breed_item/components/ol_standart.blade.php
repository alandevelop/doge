<div class="single__advantages-list-wrap single__advantages-list-wrap--simple-number" style="margin-bottom: 22px;">
    @php
        $itemsPerColumn = ceil(count($section->items) / 2);
    @endphp

    <div class="advantages-list advantages-list--simple-number">
        <div class="advantages-list__left-col">
            @foreach($section->items as $item)
                @break($loop->iteration > $itemsPerColumn)
                <div class="advantages-list__item">
                    <div class="advantages-list__number">{{$loop->iteration}}</div>
                    <div class="advantages-list__title">{{$item->ol_standart_item}}</div>
                </div>
            @endforeach
        </div>

        <div class="advantages-list__right-col">
            @foreach($section->items as $item)
                @continue($loop->iteration <= $itemsPerColumn)
                <div class="advantages-list__item">
                    <div class="advantages-list__number">{{$loop->iteration}}</div>
                    <div class="advantages-list__title">{{$item->ol_standart_item}}</div>
                </div>
            @endforeach
        </div>

    </div>
</div>