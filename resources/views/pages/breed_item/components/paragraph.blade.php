@php
    use Prismic\Dom\RichText;
@endphp

<div class="single__desc single__desc--breed-story" style="margin-bottom: 13px;">
    {!! RichText::asHtml($section->primary->paragraph) !!}
</div>