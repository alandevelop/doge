@php
    $itemsPerColumn = ceil(count($section->items) / 2);
@endphp

<div class="advantages-list advantages-list--circle-number" id="circle-number" style="margin-bottom: 17px; margin-top: 17px;">

    <div class="advantages-list__left-col">
        @foreach($section->items as $item)
            @break($loop->iteration > $itemsPerColumn)
            <div class="advantages-list__item">
                <div class="advantages-list__number">{{$loop->iteration}}</div>
                <div class="advantages-list__title">{{$item->ol_numbered_item}}</div>
            </div>
        @endforeach
    </div>


    <div class="advantages-list__right-col">
        @foreach($section->items as $item)
            @continue($loop->iteration <= $itemsPerColumn)
            <div class="advantages-list__item">
                <div class="advantages-list__number">{{$loop->iteration}}</div>
                <div class="advantages-list__title">{{$item->ol_numbered_item}}</div>
            </div>
        @endforeach
    </div>

</div>