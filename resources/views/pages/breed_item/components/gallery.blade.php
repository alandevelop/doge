<div class="photo-gallery" style="margin-bottom: 22px;">

    @foreach($data->gallery_group as $item)
        <div class="photo-gallery__col">
            <a class="photo-gallery__img-wrap" href="{{$item->gallery_item->big->url}}" data-fancybox="photo-gallery">
                <img class="photo-gallery__" src="{{$item->gallery_item->big->url}}" alt="{{$item->gallery_item->alt}}">
            </a>
        </div>
    @endforeach

    @foreach($data->video_group as $item)
        <div class="photo-gallery__col @if($item->spread) photo-gallery__col--wide @endif">
            <a class="photo-gallery__img-wrap photo-gallery__img-wrap--video" href="{{$item->url_video}}" data-fancybox="photo-gallery">
                <img class="photo-gallery__" src="{{$item->image_video->preview->url}}" alt="{{$item->image_video->alt}}">
            </a>
        </div>
    @endforeach

</div>

