<div class="care-rules__item" style="margin-bottom: 16px;">
    <div class="care-rules__desc">
        <p style="white-space: pre-line">{{$section->primary->text}}</p>
    </div>
    <div class="care-rules__icon-wrap">
        <img class="care-rules__icon" src="{{$section->primary->icon->url}}" alt="{{$section->primary->icon->alt}}">
    </div>
</div>