<div class="single__weight-list single__weight-list--volume">
    <div class="weight-list weight-list--volume">
        @foreach($section->items as $item)
            <div class="weight-list__item">
                <div class="weight-list__icon-wrap"><svg class="icon icon--paw">
                        <use xlink:href="/img/svg-sprite.svg#paw"></use>
                    </svg></div>
                <div class="weight-list__title">{{$item->ul_paw_grad_item}}</div>
            </div>
        @endforeach
    </div>
</div>