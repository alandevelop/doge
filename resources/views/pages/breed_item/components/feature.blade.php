<div class="distinctive-features__item" style="margin: 8px 0;">
    <div class="distinctive-features__title">{{$section->primary->title}}</div>
    <div class="distinctive-features__icon-wrap">
        <img class="distinctive-features__icon" src="{{$section->primary->icon->url}}" alt="{{$section->primary->icon->alt}}">
    </div>
    <div class="distinctive-features__desc">
        <p style="white-space: pre-line">{{ $section->primary->text }}</p>
    </div>
</div>