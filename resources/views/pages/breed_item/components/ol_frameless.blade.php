<div class="single__dog-list single__dog-list--style-2" style="margin-bottom: 22px;">
    <div class="dog-list dog-list--style-2">
        <ul class="dog-list__wrap">
            @foreach($section->items as $item)
                <li class="dog-list__item">
                    <div class="dog-list__number">{{$loop->iteration}}</div>
                    <div class="dog-list__desc">
                        <p style="white-space: pre-line">{{$item->ol_frameless_item}}</p>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>