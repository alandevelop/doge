<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="path/to/image.jpg">
    <meta name="theme-color" content="#000">
    <link rel="icon" href="/favicon.ico?q=1723">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon-180x180.png">
    <link rel="stylesheet" href="css/libs.min.css">
    <link rel="stylesheet" href="css/main.css">
    <title>Doge.ru | Контакты</title>

    <meta name="description" content="Doge.ru | Контакты">

    @include('components.headAndBodyEnd.head')
</head>
<body>

@include('components.header', ['index'=>false])



<section class="contacts">
    @include('components.top_line', [
        'title' => 'Контакты',
        'breadcrumbs' => [
            ['anchor' => 'Контакты', 'href'=> '']
        ]
    ])

    <div class="container">
        <div class="row contacts__row">
            <div class="col-12 col-md-6 col-lg-5 contacts__left-col">
                <h3 class="contacts__feedback-title">Связаться с нами</h3>
                <div class="contacts__desc">
                    <p>Для пиара, комерческих предложений и другой информации не связанной с функционированием системы</p>
                </div>
                <div class="contacts__small-title">Телефон</div><a class="contacts__phone" href="tel:+74991002345">+7 (499) 100-23-45</a>
                <div class="contacts__work-time">с 09:00 до 18:00, выходные: сб, вс</div>
                <div class="contacts__small-title">Электронная почта</div><a class="contacts__link contacts__link--mail" href="mailto:info@rosdolgi.ru">info@doge.ru</a>
                <div class="contacts__small-title">По вопросам сотрудничества</div><a class="contacts__link contacts__link--partner-mail" href="mailto:partner@rosdolgi.ru">partner@doge.ru</a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 contacts__right-col">
                <h3 class="contacts__feedback-title">Служба поддержка</h3>
                <div class="contacts__desc">
                    <p>Если у вас возникли трудности, нестандартные вопросы, желание узнать состояние заказа, вопросы по возврату денежных средств</p>
                </div>
                <div class="contacts__small-title">Телефон</div><a class="contacts__phone" href="tel:+74991002345">+7 (499) 100-23-45</a>
                <div class="contacts__work-time">с 09:00 до 18:00, выходные: сб, вс</div>
                <div class="contacts__small-title">Электронная почта</div><a class="contacts__link contacts__link--support" href="mailto:help@doge.ru">help@doge.ru</a><a class="contacts__btn btn js-popup" href="#request">Задать вопрос</a>
            </div>
        </div>
    </div>
</section>

@include('components.footer')
@include('components.request_popup')
@include('components.success_popup')

<!-- Scripts-->
<script src="js/libs.min.js"></script>
<script src="js/main.min.js"></script>
<script src="/js/custom.js"></script>

@include('components.headAndBodyEnd.bodyEnd')
</body>
</html>