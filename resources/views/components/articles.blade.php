<article class="articles">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="articles__title">Полезные статьи</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="articles__list owl-carousel">
                    @foreach($posts as $post)
                        <article class="articles__item">
                            <div class="articles__date">{{$post->data->pub_date_human}}</div>
                            <a class="articles__img-wrap" href="{{route('post', ['uri'=>$post->uid])}}">
                                <img class="articles__img" src="{{$post->data->main_image->url}}" alt="{{$post->data->main_image->alt}}">
                            </a>
                            <a class="articles__title-link" href="{{route('post', ['uri'=>$post->uid])}}">
                                <h3 class="articles__item-title">{{$post->data->h1}}</h3>
                            </a>
                            <div class="articles__item-desc">
                                <p>{{$post->data->short_description}}</p>
                            </div>
                            <a class="more-link" href="{{route('post', ['uri'=>$post->uid])}}">
                                <div class="more-link__text">Читать статью</div>
                                <div class="more-link__icon-wrap">
                                    <svg class="icon icon--arrow">
                                        <use xlink:href="/img/svg-sprite.svg#arrow"></use>
                                    </svg>
                                </div>
                            </a>
                        </article>
                    @endforeach
                </div>
                {{--                <a class="articles__more-btn" href="#">Еще статьи</a>--}}
            </div>
        </div>
    </div>
</article>