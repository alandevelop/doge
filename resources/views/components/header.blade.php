<header class="header @if($index) header-main @else header-other header-other--single @endif">
    <div class="container">
        <div class="header__top-line">
            <div class="row">
                <div class="col-lg-6 header__col-left d-none d-lg-flex">
                    <div class="header__work-time">Мы работаем пн-пт<i>:</i> <span>с 10:00 до 17:00</span></div>
                </div>
                <div class="col-lg-6 header__col-right d-none d-lg-flex">
                    <div class="header__support-wrap">
                        <div class="header__support-text">Тех. поддержка:</div>
                        <a class="header__support-number" href="mailto:info@doge.ru">info@doge.ru</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header__bottom-line">
            <div class="row header__row">
                <div class="col-6 header__col-left"><a class="header__logo-wrap" href="/">
                        <div class="header__logo-img-wrap"><svg class="icon icon--logo">
                                <use xlink:href="/img/svg-sprite.svg#logo"></use>
                            </svg></div>
                    </a></div>
                <div class="col-6 header__col-right">
                    <nav class="nav">
                        <ul class="nav__list">
                            <li class="nav__item">
                                <a @if(\Request::route()->getName() !== 'breeds') href="{{route('breeds')}}" @endif class="nav__link">Породы</a>
                            </li>

                            <li class="nav__item">
                                <a @if(\Request::route()->getName() !== 'contacts') href="{{route('contacts')}}" @endif class="nav__link">Контакты</a>
                            </li>
                        </ul>
                    </nav><button class="hamburger hamburger--slider js-menu" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu">
        <div class="container">
            <div class="row">
                <div class="col-12 mobile-menu__col"></div>
            </div>
        </div>
    </div>
</header>
