<div class="mfp mfp--succes-popup zoom-anim-dialog mfp-hide" id="success">
    <div class="form-result form-result--success">
        <div class="mess">
            <div class="mess__title">Success!</div>
            <div class="mess__desc">We will contact with you soon</div>
        </div>
    </div>
</div>