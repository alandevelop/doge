{{--@include('components.top_line', [--}}
{{--    'title' => 'Породы',--}}
{{--    'breadcrumbs' => [--}}
{{--        ['anchor' => 'Породы собак', 'href'=> route('breeds')]--}}
{{--    ]--}}
{{--])--}}

<div class="top-line">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="bread-crumbs">

                    <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="bread-crumbs__list">
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="bread-crumbs__item">
                            <a itemprop="item" href="/" class="bread-crumbs__link">
                                <span itemprop="name">Главная</span>
                            </a>
                            <meta itemprop="position" content="1" />
                        </li>
                        @foreach($breadcrumbs as $item)
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="bread-crumbs__item">
                                <a itemprop="item"  @isset($item['href']) href="{{$item['href']}}" @endisset class="bread-crumbs__link">
                                    <span itemprop="name">{{$item['anchor']}}</span>
                                </a>
                                <meta itemprop="position" content="{{$loop->iteration + 1}}" />
                            </li>
                        @endforeach
                    </ul>

                </nav>
            </div>
            <div class="col-12">
                <h1 class="title">{{$title}}</h1>
            </div>
        </div>
    </div>
</div>