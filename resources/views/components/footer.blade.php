<footer class="footer">
    <div class="container">
        <div class="row footer__top-row">
            <div class="footer__col">
                <a class="footer__logo-link" href="index.html">
                    <div class="footer__logo-wrap"><img class="footer__logo" src="/img/svg-sprite/logo.svg" alt=""></div>
                </a>
                <ul class="social">
                    <li class="social__item">
                        <a class="social__link social__link--vk" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 13">
                                <title>vk</title>
                                <g data-name="Слой 2">
                                    <g data-name="Слой 1">
                                        <path class="cls-1" d="M11.74,13h1.44a1.24,1.24,0,0,0,.65-.28.9.9,0,0,0,.2-.59s0-1.83.86-2.1,2,1.76,3.21,2.55a2.38,2.38,0,0,0,1.6.46l3.2,0s1.68-.1.89-1.35A10.36,10.36,0,0,0,21.4,9c-2-1.77-1.74-1.49.68-4.55,1.48-1.87,2.07-3,1.88-3.49S22.7.6,22.7.6l-3.61,0a.83.83,0,0,0-.46.07.92.92,0,0,0-.32.37A19,19,0,0,1,17,3.73c-1.61,2.59-2.25,2.73-2.52,2.57C13.85,5.93,14,4.79,14,4c0-2.51.4-3.56-.78-3.83A6.6,6.6,0,0,0,11.54,0a8.34,8.34,0,0,0-3,.29C8.14.48,7.82.91,8,.93a1.67,1.67,0,0,1,1.07.51A3.13,3.13,0,0,1,9.43,3s.22,3-.49,3.33c-.49.25-1.16-.27-2.59-2.62A22.07,22.07,0,0,1,5.06,1.17,1,1,0,0,0,4.77.79,1.74,1.74,0,0,0,4.21.57L.78.6a1.2,1.2,0,0,0-.7.22c-.17.19,0,.58,0,.58s2.68,6,5.72,9A8.47,8.47,0,0,0,11.74,13Z" />
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li class="social__item">
                        <a class="social__link social__link--twi" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 15">
                                <title>twi</title>
                                <g data-name="Слой 2">
                                    <g data-name="Слой 1">
                                        <path class="cls-1" d="M0,13.3A11.32,11.32,0,0,0,6,15,10.85,10.85,0,0,0,17.07,4.22c0-.16,0-.32,0-.48a7.85,7.85,0,0,0,1.94-2,8,8,0,0,1-2.24.59A3.86,3.86,0,0,0,18.48.28,8.11,8.11,0,0,1,16,1.19,4,4,0,0,0,13.15,0,3.83,3.83,0,0,0,9.26,3.78a3.57,3.57,0,0,0,.1.87,11.2,11.2,0,0,1-8-4A3.74,3.74,0,0,0,2.53,5.75,3.93,3.93,0,0,1,.76,5.27v.05A3.83,3.83,0,0,0,3.89,9a4.34,4.34,0,0,1-1,.13,3.54,3.54,0,0,1-.73-.07,3.88,3.88,0,0,0,3.64,2.63A7.92,7.92,0,0,1,.93,13.35,7.25,7.25,0,0,1,0,13.3Z" />
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li class="social__item">
                        <a class="social__link social__link--fb" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8 17">
                                <title>fb</title>
                                <g data-name="Слой 2">
                                    <g data-name="Слой 1">
                                        <path class="cls-1" d="M6,2.92H8V0H5.31A3.33,3.33,0,0,0,1.7,3.56V5.51H0v3H1.7V17H5.27V8.51H7.69l.31-3H5.27V3.74A.73.73,0,0,1,6,2.92Z" />
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li class="social__item">
                        <a class="social__link social__link--ok" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 18">
                                <title>ok</title>
                                <g data-name="Слой 2">
                                    <g data-name="Слой 1">
                                        <path class="cls-1" d="M10.67,3.55A4.48,4.48,0,0,1,7.05,8.87c-5.94,1.25-8.5-7-2.17-8.71A4.75,4.75,0,0,1,10.67,3.55ZM8.27,4A2.35,2.35,0,0,0,5.4,2.38,2.21,2.21,0,1,0,6.6,6.63,2.19,2.19,0,0,0,8.27,4ZM2.76,9.63a7.1,7.1,0,0,0,6.56-.05l.31-.19A1.74,1.74,0,0,1,10.73,9c.86.05,2.59,1.41-.46,2.84a7.07,7.07,0,0,1-2,.7c-.27.05-.57.11-1,.23a25.62,25.62,0,0,0,2.48,2.36l.83.74c1.14,1.05.17,2.24-1,2.12-.74-.08-3.06-2.36-3.48-2.83a7.57,7.57,0,0,0-1.56,1.31c-.72.71-1.46,1.44-2,1.51a1.22,1.22,0,0,1-1-2.2l3.27-3a5.12,5.12,0,0,0-.94-.23l-.7-.16C-1.62,11,.08,8.88,1.45,9a2.31,2.31,0,0,1,1,.43Z" />
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                </ul>
                <div class="feedback">
                    <div class="feedback__item">
                        <div class="feedback__title">Почта</div><a class="feedback__link" href="mailto:info@doge.ru">info@doge.ru</a>
                    </div>
{{--                    <div class="feedback__item">--}}
{{--                        <div class="feedback__title">Телефон</div>--}}
{{--                        <a class="feedback__link" href="tel:88001003434">8 800 100 34 34</a>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="footer__col">
                <nav class="footer-nav">
                    <div class="footer-nav__title">Меню</div>
                    <ul class="footer-nav__list">
                        @if(url()->current() !== route('index'))
                            <li class="footer-nav__item"><a class="footer-nav__link" href="/">Главная</a></li>
                        @endif
                        @if(url()->current() !== route('breeds'))
                            <li class="footer-nav__item"><a class="footer-nav__link" href="{{route('breeds')}}">Породы</a></li>
                        @endif
                        @if(url()->current() !== route('contacts'))
                            <li class="footer-nav__item"><a class="footer-nav__link" href="{{route('contacts')}}">Контакты</a></li>
                        @endif
                    </ul>
                </nav>
            </div>
            <div class="footer__col">
                <nav class="footer-nav">
                    <div class="footer-nav__title">Категории</div>
                    <ul class="footer-nav__list">
                        @foreach($categories['cat_purpose'] as $category)
                            @php
                                $type = $category->type;
                                $id = $category->id;

                                $currUrl = url()->full();
                                $isRecursive = $currUrl === route('breeds')."?$type=$id";
                            @endphp

                            @if(!$isRecursive)
                                <li class="footer-nav__item">
                                    <a class="footer-nav__link" href="/poroda/{{$category->uid}}">{{$category->data->name}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </nav>
            </div>
            <div class="footer__col">
                <nav class="footer-nav">
                    <div class="footer-nav__title">Порулярные породы</div>
                    <div class="footer-nav__list-wrap">
{{--                        @php $itemsPerColumn = ceil(count($popular) / 2); @endphp--}}
                        @php $itemsPerColumn = 6; @endphp
                        <ul class="footer-nav__list">
                            @foreach($popular as $item)
                                @break($loop->iteration > $itemsPerColumn)

                                @php
                                    $currUrl = url()->current();
                                    $isRecursive = $currUrl === route('breed', ['uri'=>$item->uid]);
                                @endphp

                                @if(!$isRecursive)
                                    <li class="footer-nav__item">
                                        <a class="footer-nav__link"
                                           href="{{route('breed', ['uri'=>$item->uid])}}"
                                           style="line-height: 15px; display: inline-block;"
                                        >{{$item->data->breed_name}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <ul class="footer-nav__list">
                            @foreach($popular as $item)
                                @continue($loop->iteration <= $itemsPerColumn)
                                @break($loop->iteration >= $itemsPerColumn * 2)

                                @php
                                    $currUrl = url()->current();
                                    $isRecursive = $currUrl === route('breed', ['uri'=>$item->uid]);
                                @endphp

                                @if(!$isRecursive)
                                    <li class="footer-nav__item">
                                        <a class="footer-nav__link"
                                           href="{{route('breed', ['uri'=>$item->uid])}}"
                                           style="line-height: 15px; display: inline-block;"
                                        >{{$item->data->breed_name}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row footer__bottom-row">
            <div class="col-12 col-md-6">
                <a rel="nofollow" class="footer__agreement" href="{{route('agreement')}}">Пользовательское соглашение</a>
                <a rel="nofollow" class="footer__politics" href="{{route('privacy_policy')}}">Политика конфиденциальности</a>
            </div>
            <div class="col-12 col-md-6 footer__col-right">
                <div class="footer__copyright">Все права защищены. Copyright 2020</div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a class="up-btn" href="#"> <svg class="icon icon--arrow-up">
                        <use xlink:href="/img/svg-sprite.svg#arrow-up"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</footer>
