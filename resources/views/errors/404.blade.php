<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/custom.css{{assets_cache_key()}}">
</head>
<body>
    <div class="container">
        <div class="cnt">
            <a href="/" class="main">Doge.ru</a>
            <h1>Упс, это 404</h1>
            <p class="descr">Кажется вы нажали куда-то не туда
                и перешли на не существующую страницу</p>
            <a href="{{ url()->previous() }}" class="btn">Вернуться</a>

            <p class="copyright">Все права защищены. Copyright {{date('Y')}}</p>
        </div>
    </div>
</body>



<style>
    body {
        background-image: url('/img/errors/404_bg.jpg');
        background-repeat: no-repeat;
        background-position: right bottom;
    }
    
    .cnt {
        display: flex;
        flex-direction: column;
        height: 100vh;
    }

    .main {
        padding-top: 80px;
        font-family: Gilroy;
        font-style: normal;
        font-weight: bold;
        font-size: 32px;
        line-height: 40px;
        color: #183A5F;
    }

    h1 {
        font-family: Gilroy;
        font-style: normal;
        font-weight: bold;
        font-size: 72px;
        line-height: 72px;
        color: #6D6D6D;
        margin-bottom: 20px;
    }

    .descr {
        font-family: Gilroy;
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        line-height: 28px;
        color: #979797;
        width: 100%;
        max-width: 384px;
        margin-bottom: 40px;
    }

    .btn {
        width: 176px;
        padding: 20px 28px;
        margin-bottom: auto;
    }

    .copyright {
        font-family: Gilroy;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        color: #A2A2A2;
        margin-top: auto;
        margin-bottom: 60px;
    }

    @media screen and (max-width: 1000px) {
        body {
            background: none;
        }

        h1 {
            font-size: 48px;
        }

        .btn {
            margin-bottom: 20px;
        }

        .main {
            padding-bottom: 20px;
        }
    }


</style>
</html>