////////// Responsive
// Breackpoints
let	xl 	=  '(max-width: 1140px)',
		lg 	=  '(max-width: 991px)',
		md 	=  '(max-width: 768px)',
		sm 	=  '(max-width: 575px)',
		xsm	=  '(max-width: 375px)',
		MQ  =  $.mq.action;
// MediaQueries
// XSM
MQ(xsm, function () {
}, function () {
});

// SM
MQ(sm, function () {
	// $('.ban-checker-result__mobile-img-wrap').append( $('.js-img'));
}, function () {
	// $('.ban-checker-result__img-wrap').append( $('.js-img'));
});

// MD
MQ(lg, function () {
	$('.mobile-menu__col').append( $('.nav'));
	$('.mobile-menu__col').append( $('.header__work-time'));
	$('.mobile-menu__col').append( $('.header__support-wrap'));
	$('.single-main__mobile-container').append($('.single-main__top-line'))
}, function () {
	$('.header__bottom-line .header__col-right').prepend( $('.nav'));
	$('.header__top-line .header__col-left').prepend( $('.header__work-time'));
	$('.header__top-line .header__col-right').prepend( $('.header__support-wrap'));
	$('.single-main__right-col').prepend($('.single-main__top-line'))
	// $('.tax-checker-step__inn-field-wrap').append( $('.tax-checker__next-btn'));
});





////////// Common functions

let validator;

jQuery.validator.setDefaults({
	rules:{
		name:{
			required: true,
			minlength: 3,
		},
		select:{
			required: true
		},
		email:{
			required: true,
			email: true
		},
	},
	messages:{
		check: 'Обязательное поле',
		name: '',
		surname: '',
		phone: '',
		email: ''
	},
	errorPlacement: function(error, element) {
		
	},
	submitHandler: function() {
		// Write here your function Handler
		console.log('Submit');
	}
});

// validator init
$('form').each( function() {
	validator = $(this).validate();
});

// Popup opener
$('.js-popup').click(function (event) {
	event.preventDefault();
	let popupID = $(this).attr('href');

	mfpPopup(popupID);
});

// Mobile menu toggle
$('.js-menu').click(function () {
	$(this).toggleClass('is-active');
	$('.mobile-menu').toggleClass('opened')
	$('.hamburger').prop('disabled', true);
	setTimeout(() => {
		$('.hamburger').prop('disabled', false);
	}, 1000);
	
	if ($('.header-main .mobile-menu').is(':visible')) {
		$('.header-main .icon--logo').css('fill', '#183A5F')
	}
	if (!$('.header-main .mobile-menu').hasClass('opened')) {
		$('.header-main .icon--logo').css('fill', '#fff')
	}
});

// Phone input mask
$('input[type="tel"]').inputmask({
	mask: '+7 (999) 999-99-99',
	showMaskOnHover: false,
});

// Init sliders
let owlNavText = '<svg class="icon icon--arrow"><use xlink:href="/img/svg-sprite.svg#arrow"></use></svg>';
let articleList = $('.articles__list');
let owlSettings = {
	loop: true,
	items: 4,
	margin: 24,
	nav: true,
	navText: [owlNavText, owlNavText],
	responsiveClass:true,
		responsive:{
				0:{
						items:1,
				},
				400:{
						items:2,
				},
				767:{
						items:3,
				},
				992:{
						items:4,
				}
		}
}
function articleSlider(){
	MQ(sm, function () {
		reInitOwl();
	}, function () {
		initOwl();
	});
}
articleSlider();

function initOwl() {
	articleList.owlCarousel(owlSettings).addClass('owl-carousel').trigger('refresh.owl.carousel');
}

function reInitOwl() {
	articleList.trigger('destroy.owl.carousel').removeClass('owl-carousel');
}

let slickPrevText = '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style=""><svg class="icon icon--slick-arrow"><use xlink:href="/img/svg-sprite.svg#slick-arrow"></use></svg></button>';
let slickNextText = '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><svg class="icon icon--slick-arrow"><use xlink:href="/img/svg-sprite.svg#slick-arrow"></use></svg></button>';
$('.single-main__slider').slick({
  slidesToShow: 1,
	slidesToScroll: 1,
  prevArrow: slickPrevText,
	nextArrow: slickNextText,
	arrows: true,
	dots: false,
	infinite: true,
	asNavFor: '.single-main__slider-nav',
	focusOnSelect: true,
	draggable: false,
	fade: false,
});

let sliderNav = $('.single-main__slider-nav');
sliderNav.slick({
  slidesToShow: 4,
	slidesToScroll: 1,
  asNavFor: '.single-main__slider',
	dots: false,
	arrows: false,
	focusOnSelect: true,
	infinite: true,
});

function iframeHeightChecker() {
	let sliderItem = $('.single-main__slider-item');
	let sliderItemHeight = sliderItem.height();
	let sliderIframe = sliderItem.find('iframe');
	sliderIframe.height(sliderItemHeight);
}

iframeHeightChecker();

$(window).on('resize', function(){
	iframeHeightChecker();
});

// Smooth scroll 
$('.content-nav__link, .content-nav__list-heading, .breeds-info-card').click(function(event){
	event.preventDefault();
	let el = $(this).attr('href');
	let headerTopOffset = 75;

	if ($(el).length) {
		$('body, html').animate({
			scrollTop: $(el).offset().top - headerTopOffset}, 700);
		
		return false;
	}
});

// Smooth scroll to top
let upBtn = $('.up-btn');

function smoothScrollToTop(){

	if ($(window).scrollTop() > 300) {
		upBtn.addClass('show');
	} else {
		upBtn.removeClass('show');
	}
}

// Up btn listener
upBtn.on('click', function(e) {
	e.preventDefault();
	$('html, body').animate({scrollTop:0}, 700);
});

// Content Navigation
function contentNav() {
	let scrollTop = $(window).scrollTop();
	let headerHeight = 18;
	let contentNav = $('.content-nav');
	let contentNavCol = $('.single__col-right');
	let contentNavColTopOffset = contentNavCol.offset().top;
	let contentNavColOffset = contentNavColTopOffset - headerHeight;
	let footer = $('.footer');
	let footerOffset = footer.offset().top;
	let bottomBreakPoint = footerOffset - (contentNav.outerHeight() + 78 + headerHeight) 
	let contentNavPos = footerOffset - (((contentNav.outerHeight()) * 2) + footer.outerHeight() - 58)
	// let contentNavPos = footerOffset - (((contentNav.outerHeight()) * 2) + footer.outerHeight() - 68)
	

	if (bottomBreakPoint < scrollTop) {
		contentNav.css('top', contentNavPos)
		contentNav.addClass('content-nav--bottom')
	}else{
		contentNav.removeClass('content-nav--bottom')
		contentNav.css('top', 18)
	}

	if (scrollTop > contentNavColOffset) {
		contentNav.addClass('content-nav--scrolled');
	} else {
		contentNav.removeClass('content-nav--scrolled');
	}
	
}

//fix header
function fixHeader() {
	var scrollTop = $(window).scrollTop();
	var header = $('.header');
	var headerHeight = header.height();

	if (scrollTop > headerHeight) {
			header.addClass('header--scrolled');
	} else {
			header.removeClass('header--scrolled');
	}
}
// on start page
fixHeader();
// on scroll
$(window).scroll(function() {
	fixHeader();
});

// Function conditions
function functionConditions(){

	if ($('.content-nav').length) {
		MQ(lg, () => {
		}, ()=> {
			contentNav();
		});
	}

	if ($('.up-btn').length) {
		smoothScrollToTop();
	}

}

functionConditions()

// function hideLetterItem() {
// 	let contentList = $('.breeds-letter-content__list li');
// 	let contentItem = $('.breeds-letter-content__parent-item')

// 	contentItem.each(function(){
// 		let limit = 16;

// 		for (let i = limit; i < $(this).find(contentList).length; i++) {
// 			$(this).find(contentList).eq(i).hide();
// 		}

// 	});
// }

// hideLetterItem();

// Creat custom column
function creatColumn() {
	let contentItem = $('.breeds-letter-content__parent-item')

	contentItem.each(function(){

		let listLength = $(this).find('.breeds-letter-content__item').length;
		let columnLength = Math.round(listLength / 2);
		let heightLeftColumn = 0;

		for (let i = 0; i < columnLength; i++) {
			let item = $(this).find('.breeds-letter-content__item')[i];
			$(item).addClass('breeds-letter-content__item--left-col');
			heightLeftColumn += $(item).outerHeight();
		}

		for (let i = columnLength; i < listLength; i++) {
			let item = $(this).find('.breeds-letter-content__item')[i];
			$(item).addClass('breeds-letter-content__item--right-col');
		}
		
		MQ(lg, ()=> {
			$(this).find('.breeds-letter-content__list').css('max-height', '100%');
		}, ()=> {
			$(this).find('.breeds-letter-content__list').css('max-height', heightLeftColumn);
		});
		
	});
}
creatColumn();
// Letter content filter
function letterContentFilter() {

	let letterLink = $('.breeds-letter__link');
	let letterLinkContent = $('.breeds-letter-content__parent-item')

	letterLink.click(function(e){
		e.preventDefault();

		let dataHref = $(this).data('href');
		let target = $('.breeds-letter-content__parent-item[data-id="'+dataHref+'"]');

		if (target.length) {
			let removeBtn = $('.breeds-letter-content__title-wrap').find('.breeds-letter-content__remove-btn');

			letterLink.removeClass('breeds-letter__link--active')
			$(this).addClass('breeds-letter__link--active')
			letterLinkContent.not(target).hide(300);
			target.show(300).parent().parent().addClass('show');
			target.find('.breeds-letter-content__title-wrap').append($(removeBtn));
			target.find('.breeds-letter-content__title-wrap').show(300);

			console.log(target);
		}
	});
}

letterContentFilter();

function letterFilterRemover() {
	$('.breeds-letter-content__remove-btn').click(function(){
		$('.breeds-letter-content__parent-item').show(300);
		$('.breeds-letter__link').removeClass('breeds-letter__link--active');
		$('.breeds-letter-content').removeClass('show');
	});
}
letterFilterRemover();

// function retinaImg(){
// 	let img = $('.articles__item .articles__img-wrap img');
// 	let imgSrc = img.attr('src');
// 	let imgArr = imgSrc.split('/')
// 	imgArr.splice(1, 0, 'mobile/');
// 	imgArr.forEach(element => {
// 		// console.log(element);
// 		console.log(element.split());
// 	});
// 	console.log(imgArr);
// 	// newSrc.join();
// 	// img.attr('src', newSrc);
	
// }
// retinaImg();

// jQ form styler
function inputStyler(fn) {
	let elements = $('select');
	if (fn !== 'refresh') {
		elements.styler({
			selectSearch: false,
			selectSmartPositioning: false,
		});
	} else {
		elements.trigger('refresh');
	}
}

setTimeout(function() {
	inputStyler();
}, 10);

// Search popup toggle
function search() {
	let searchInput = $('input[name="search"]');
	let searchPopup = $('.search__list');
	
	$('.search__link').click(function(e){
		e.preventDefault()
		let linkText = $(this).text();

		searchInput.val(linkText);
		searchPopup.hide(300);

		// setTimeout(() => {
		// 	searchPopup.addClass('search__list--disabled');
		// }, 300);

	});

}
search();

// Breeds Filter
$('.breeds-filter').submit(function(e){
	e.preventDefault();

	let form = $(this);
	let formData = {};
	formData.data = {};
	let formValue = [];
	let resultList = $('.breeds-filter-result__list');

	$('.breeds-filter-result').show(300);
	// Serialize
	form.find('select').each(function () {
		let name = $(this).attr('name');
		let value = $(this).val();
		
		formData.data[name] = {
			value: value,
		};

		let template = `<li class="breeds-filter-result__item">${formData.data[name].value}<div class="breeds-filter-result__icon-wrap"><svg class="icon icon--close"><use xlink:href="/img/svg-sprite.svg#close"></use></svg></div></li>`;
		if (formData.data[name].value !== '') {
			formValue.push(template)
		}

		// $.ajax({
		// 	type: 'GET',
		// 	// url: '',
		// 	dataType: 'json',
		// 	data: formData,
		// }).done(function (data) {
		// 	console.log(data);
		// 	if (data.status === 'success') {

		// 	} else {

		// 	}
		// });
		// return false;
		
	});
	resultList.html(formValue);

});


function removeFilterItem({target}){

	if (target.classList.contains('icon--close') || target.hasAttribute('xlink:href')) {
		target.closest('.breeds-filter-result__item').remove();
	}
	if(!$('.breeds-filter-result__list').children().length){
		hideResultList();
	}
}

$('.breeds-filter-result').click(removeFilterItem);

$('.breeds-filter-result__remove-btn').click(function(){

	$('.breeds-filter-result__item').remove();
	hideResultList();

});

function hideResultList(){
	$('.breeds-filter-result').hide(300);
}
function dogCategoryHeightChecker() {
	let itemWidth = $('.dog-category__item').width();
	$('.dog-category__item').height(itemWidth);

}
// dogCategoryHeightChecker()
////////// Ready Functions
$(document).ready(function () {
	
	$(window).scroll(function() {
		functionConditions();
	});

});

$(window).on('resize', function(){
	// dogCategoryHeightChecker();
})
////////// Load functions
$(window).on('load', function () {
	sliderNav.on('afterChange', function(event){
		if($('iframe').length){
			callPlayer("single-main__slider-item","pauseVideo");
		}
	});
	
	sliderNav.on('beforeChange', function(event){
		if($('iframe').length){
			callPlayer("single-main__slider-item","pauseVideo");
		}
	});
});

/////////// mfp popup - https://dimsemenov.com/plugins/magnific-popup/
let mfpPopup = function (popupID, source) {
	$.magnificPopup.open({
		items: {
			src: popupID,
		},
		type: 'inline',
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		closeMarkup: '<button type="button" class="mfp-close">&times;</button>',
		mainClass: 'mfp-fade-zoom',
		// callbacks: {
		// 	open: function() {
		// 		$('.source').val(source);
		// 	}
		// }
	});
};

function callPlayer(frame_id, func, args) {
	if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
	let customClassName = ('.' + frame_id);
	var iframe = document.querySelectorAll(customClassName);

	iframe.forEach(element => {
		if (element) {
		element = element.getElementsByTagName('iframe')[0];
		}

		if (element) {
			// Frame exists,
			element.contentWindow.postMessage(JSON.stringify({
				"event": "command",
				"func": func,
				"args": args || [],
				"id": frame_id
			}), "*");
		}

	});
	
}

// Animation on scroll
// let anim = new AnimationOnScroll({
// 	jsonUrl: '/js/animations.json',
// 	aosOptions: {
// 		offset: 50,
// 		duration: 700,
// 		once: true,
// 	},
// });

// // Init animations
// anim.init();