function cl(data) {
    console.log(data);
}

$(document).ready(function() {
    function showSearchItems(res) {
        if(!res.length) return;

        var items = '';
        for (var i = 0; i < res.length; i++) {
            if(res[i]['is_active']) {
                items += `<li class="search__item"><a class="search__link" href="/breed/${res[i]['uid']}">${res[i]['name']}</a></li>`;
            }
        }

        $('.search__list').html(items).show();
    }

    function make_input_searchable(searchUrl, form, input, clickableForSearch, searchIcon, searchList, callback, hideResults) {
        function search(query) {
            searchIcon.hide();
            $('.custom_loading').show();

            $.ajax({
                url: searchUrl,
                type: 'GET',
                data: { "query":  query},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    searchIcon.show();
                    $('.custom_loading').hide();

                    callback(res);
                }
            });
        }

        // поиск on form submit
        form.submit(function(e) {
            e.preventDefault();
            let query = $('form.search input').val();
            if(query.length < 2) {
                searchList.hide();
                return;
            }
            search(query);
        });

        // живой поиск по вводу в поле
        let timerId;
        input.on('input', function() {
            clearTimeout(timerId);

            let query = $(this).val();
            if(query.length < 2) {
                searchList.text('').hide();
                return;
            }

            timerId = setTimeout(search, 400, query);
        });

        // поиск по нажатию на...
        clickableForSearch.click(function(e) {
            if($(this).val().length < 2) {
                searchList.hide();
                return;
            }
            search($(this).val());
        });

        if(hideResults) {
            // скрыть результаты поиска по нажитию на esc
            $(document).keyup(function(e) {
                if (e.key === "Escape") {
                    searchList.hide();
                }
            });
            // скрыть результаты при клике за их пределами
            $(document).click(function(event) {
                if ($(event.target).closest(form).length) return;	//Игнорировать клики внутри
                searchList.hide();

                event.stopPropagation();
            });
        }
    }

    make_input_searchable(
        '/search-breed',
        $('form.search'),
        $('form.search input'),
        $('form.search input, .search__icon-wrap'),
        $('.search__icon-wrap'),
        $('.search__list'),
        showSearchItems,
        true
    );

    function showNonFormResults(res) {
        if(!res.length) return;

        var items = '';
        for (var i = 0; i < res.length; i++) { // columns
            items += `<div class="col-12 col-sm-6 col-lg-3 breeds__col"><ul class="breeds__list breeds__list--letter-a">`;

            for (var s = 0; s < res[i].length; s++) {
                if(res[i][s]['is_active']) {
                    items += `<li class="breeds__item"><a class="breeds__link" href="/breed/${res[i][s]['uid']}">${res[i][s]['name']}</a></li>`;
                } else {
                    items += `<li class="breeds__item"><a class="breeds__link">${res[i][s]['name']}</a></li>`;
                }
            }

            items += `</ul></div>`;
        }

        $('.indexColumnsSearchResults').html(items).show();
    }

    make_input_searchable(
        '/search-breed-column',
        $('form.breeds-search'),
        $('form.breeds-search input'),
        $('.breeds-search__icon-wrap'),
        $('.breeds-search__icon-wrap'),
        $('.indexColumnsSearchResults'),
        showNonFormResults,
        false
    );

    if (window.matchMedia &&
        window.matchMedia('(prefers-color-scheme: dark)').matches) {

        var link = document.querySelector("link[rel*='icon']");
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = '/favicon-white.ico'; // адрес вашего светлого favion'а для темной темы

        document.getElementsByTagName('head')[0].appendChild(link);
    }

});